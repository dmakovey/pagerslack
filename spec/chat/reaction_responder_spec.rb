# frozen_string_literal: true

require 'spec_helper'
require 'semantic_logger'
require './lib/chat/reaction_responder'
require 'slack'

describe Chat::ReactionResponder do
  let(:event_data) { { 'user' => 'member', 'reaction' => 'eyes', 'item' => { 'ts' => Time.new(2020).to_f } } }
  let(:event) { double('event') }

  subject(:responder) { described_class.new(event, event_data) }

  describe '#position' do
    before do
      allow(Store).to receive(:get).and_return(event_data['item']['ts'])
      allow(event).to receive(:set?).and_return(false)
      allow(event).to receive(:set)
      allow(responder).to receive(:username).and_return('user')
    end

    it 'sends the right message' do
      expect(Chat::Client.instance).to receive(:message).with('user thank you for looking into this! :heart:', ts: event_data['item']['ts'])

      responder.respond
    end
  end
end
