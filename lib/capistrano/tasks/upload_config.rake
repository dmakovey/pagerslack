# frozen_string_literal: true

namespace :deploy do
  task :upload_config do
    on roles(:all) do |_host|
      within shared_path.to_s do
        upload! 'oncall.csv', 'new_oncall.csv'
        upload! 'holidays.yml', 'holidays.yml'
        upload! 'exclusions.yml', 'exclusions.yml'
      end
    end
  end

  task :csv_refresh do
    on roles(:all) do |_host|
      within current_path.to_s do
        execute(:bundle, 'exec bin/csv_refresh_remote')
      end
    end
  end

  after 'deploy:check:make_linked_dirs', :upload_config
  after :log_revision, :csv_refresh
end
