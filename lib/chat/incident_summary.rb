# frozen_string_literal: true

require './lib/chat/client'

module Chat
  class IncidentSummary
    include Concurrent::Async

    def initialize(reporter:, pinged_members:, ack_member:, ts:)
      super()
      @reporter = reporter
      @pinged_members = pinged_members
      @ack_member = ack_member
      @ts = ts
    end

    def post
      client.message("*Incident Summary*\n\n#{reporter_output}\n\n#{time_to_response_output}", ts: @ts)
    end

    private

    def time_to_response_output
      "Time #{@ack_member ? 'to' : 'without'} response: <#{(@pinged_members.size * Config.next_ping_wait) / 60} minutes."
    end

    def reporter_output
      "Reported by: <@#{@reporter}>"
    end

    def client
      Chat::Client.instance
    end
  end
end
