# frozen_string_literal: true

require 'date'
require 'yaml'

class ExclusionChecker
  EXCLUSIONS_FILENAME = './exclusions.yml'

  def initialize
    @exclusions = YAML.load(File.read(EXCLUSIONS_FILENAME))
  end

  def excluded?(email)
    @exclusions.any?(email)
  end
end
