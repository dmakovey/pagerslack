# frozen_string_literal: true

require 'csv'
require 'fileutils'
require './lib/exclusion_checker'

class CsvParser
  CSV_HEADERS = %w[name slack email weekend].freeze
  ONCALL_CSV = './oncall.csv'

  def initialize(file:)
    @new_csv_export = file
    @backup_csv = "#{ONCALL_CSV}.backup-#{Time.now.to_i}"
    @exclusion_checker = ExclusionChecker.new
    @final = []
  end

  def parse
    FileUtils.cp(ONCALL_CSV, @backup_csv)

    CSV.foreach(@new_csv_export, liberal_parsing: true) do |n|
      next if n[0] == 'name'
      next if @exclusion_checker.excluded?(n[11])
      next unless n[10]
      next unless n[6] == 'Backend/Distribution/Fullstack'

      process_row(n)
    end

    @final.sort_by! { |c| c[0] }.unshift(CSV_HEADERS)
    File.write(ONCALL_CSV, @final.map(&:to_csv).join)

    system(diff_command)
    puts 'Done.'
  end

  protected

  def diff_command
    "diff -u #{@backup_csv} #{ONCALL_CSV}"
  end

  def process_row(num)
    name = num[0].strip.delete('"')
    found = CSV.read(ONCALL_CSV).find { |f| f[0] == name }
    slack_id = found&.dig(1) || prompt_for_slack_id(name)

    @final << [name, slack_id, num[11]&.strip, num[10]&.strip]
  end

  def prompt_for_slack_id(name)
    print "Please enter #{name} Slack ID: "

    gets.strip
  end
end
