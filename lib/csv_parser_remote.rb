# frozen_string_literal: true

require 'slack-ruby-client'
require 'csv'
require 'fileutils'
require './lib/chat/client'
require './lib/csv_parser'

class CsvParserRemote < CsvParser
  def initialize(file:)
    super(file: file)
    @invitees = []
  end

  def parse
    super

    client.invite(@invitees) if @invitees.any?
  end

  protected

  def diff_command
    "diff -u #{@backup_csv} #{ONCALL_CSV} | diffstat | tail -n 1"
  end

  def process_row(num)
    name = num[0].strip.delete('"')
    found = CSV.read(ONCALL_CSV).find { |f| f[0] == name }
    slack_id = found&.dig(1) || get_slack_id(num[11]&.strip)

    raise StandardError, "Slack ID for #{num[11]} not found. Name: (#{name})" unless slack_id

    @final << [name, slack_id, num[11]&.strip, num[10]&.strip]
  end

  def get_slack_id(email)
    slack_id = client.get_user_id(email: email)
    @invitees << slack_id

    slack_id
  end

  def client
    Chat::Client.instance
  end
end
